package ru.ustits;

/**
 * @author ustits
 */
public class ClassA {

  public int sum(int a, int b) {
    return a + b;
  }

  public String decorate(String text) {
    return "fluffy ".concat(text.concat(" is more fluffy"));
  }
}
