package ru.ustits;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author ustits
 */
public class ClassB {

  public void writeToFile(String text, String filePath) throws IOException {
    Path path = Paths.get(filePath);
    Files.write(path, text.getBytes());
  }
}
