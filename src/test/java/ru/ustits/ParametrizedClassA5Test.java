package ru.ustits;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;

/**
 * @author ustits
 */
class ParametrizedClassA5Test {

  private ClassA classA = new ClassA();

  @ParameterizedTest
  @ValueSource(ints = {1, 2, 3})
  void test(int argument) {
    assertEquals(argument + 1, classA.sum(argument, 1));
  }

  @ParameterizedTest
  @MethodSource("range")
  void test2(int argument) {
    assertEquals(argument + 1, classA.sum(argument, 1));
  }

  private static IntStream range() {
    return IntStream.range(0, 20);
  }
}
