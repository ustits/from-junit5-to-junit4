package ru.ustits.extensions;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

/**
 * @author ustits
 */
public class CustomExtension implements BeforeEachCallback, AfterAllCallback {

  @Override
  public void afterAll(ExtensionContext extensionContext) throws Exception {
    System.out.println("after");
  }

  @Override
  public void beforeEach(ExtensionContext extensionContext) throws Exception {
    System.out.println("before");
  }
}
