package ru.ustits.extensions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

/**
 * @author ustits
 */
@ExtendWith(CustomExtension.class)
@ExtendWith(AnotherExtension.class)
class TestWithExtension {

  @Test
  void dullTest() {
    System.out.println("in test");
    /*
    Prints:
      before
      in test
      after test execution
      after
     */
  }

  @ExtendWith(SingleExtension.class)
  @Test
  void singleExtension() {
    System.out.println("single test");
    /*
    Prints
      before
      single before
      single test
      after test execution
      after
     */
  }
}
