package ru.ustits.extensions;

import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

/**
 * @author ustits
 */
public class AnotherExtension implements AfterTestExecutionCallback {

  @Override
  public void afterTestExecution(ExtensionContext extensionContext) throws Exception {
    System.out.println("after test execution");
  }
}
