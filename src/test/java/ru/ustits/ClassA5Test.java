package ru.ustits;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author ustits
 */
class ClassA5Test {

  private ClassA classA;

  @BeforeEach
  void setUp() {
    classA = new ClassA();
  }

  @Test
  void testDecorate() {
    String result = classA.decorate("test");
    assertTrue(result.contains("test"));
  }

  @Test
  void testThrowNPE() {
    assertThrows(NullPointerException.class,
            () -> classA.decorate(null));
  }

  @Disabled
  @Test
  void testBrokenTest() {
    throw new RuntimeException();
  }
}