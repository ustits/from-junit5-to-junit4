package ru.ustits;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author ustits
 */
class ClassA5RepeatedTest {

  private ClassA classA;

  @BeforeEach
  void setUp() {
    classA = new ClassA();
  }

  @RepeatedTest(10)
  void testRepeatedly() {
    Random random = new Random();
    int a = random.nextInt();
    int b = random.nextInt();
    assertEquals(a + b, classA.sum(a, b));
  }

  @RepeatedTest(value = 10, name =
          "here {displayName} goes with {currentRepetition}/{totalRepetitions}")
  void testRepeatedly2() {
    Random random = new Random();
    int a = random.nextInt();
    int b = random.nextInt();
    assertEquals(a + b, classA.sum(a, b));
  }
}
